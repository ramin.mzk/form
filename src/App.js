import React, { Component } from "react";
import List from "./Componet/List/List";
import ShowList from "./Componet/ShowList";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      DisplayPage: <></>
    };
  }

  updateDisplay = (page, users, upDateUesr) => {
    this.setState({
      DisplayPage: (
        <ShowList
          Page={page}
          Users={users}
          upDateUsers={upDateUesr}
        />
      )
    });
  };

  render() {
    return (
      <>
        <div id="wrapper">
          <div className="container">
            <div className="row">
              <div className="col">
                <div className="card">
                  <div className="card-header">مدیریت وظایف</div>
                  <List  //<List /> ===> <div></div> </List>
                    Display={this.state.DisplayPage}
                    updatePage={this.updateDisplay}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
