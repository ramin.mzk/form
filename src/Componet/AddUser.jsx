import React, { Component } from "react";
import { getCategories } from "../Utility/Category";
// import UpdatePage from '../App'

export default class AddUser extends Component {
  constructor(props) {
    super(props);
    this.category = getCategories();
    this.state = {};
  }

  handelSave = () => {
    const form = document.querySelector("#expense_form");
    
        this.props.upDateUesrs({
          title: form.expense_title.value,
          category: form.expense_category.value,
          amount: form.expense_amount.value,
          type: form.expense_type.value
        });
    };

  render() {
    return (
      <form id="expense_form">
        <div className="form-group">
          <label htmlFor="expense_title">عنوان</label>
          <input
            type="text"
            className="form-control"
            name="expense_title"
            id="expense_title"
            placeholder="عنوان وظیفه"
          />
        </div>
        <div className="form-group">
          <label htmlFor="expense_category">دسته بندی</label>
          <select
            className="form-control"
            name="expense_category"
            id="expense_category"
          >
            {Object.keys(this.category).map((key, index) => (
              <option key={index} value={key}>
                {this.category[key]}
              </option>
            ))}
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="expense_amount">مبلغ</label>
          <input
            type="number"
            className="form-control"
            name="expense_amount"
            id="expense_amount"
            placeholder="مبلغ به تومان"
          />
        </div>
        <div className="form-group">
          <label className="ios7-switch">
            <input
              id="expense_type"
              type="radio"
              name="expense_type"
              value="1"
              defaultChecked="true"
            />
            <span></span>
          </label>
          هزینه
          <label className="ios7-switch">
            <input
              id="expense_type"
              type="radio"
              name="expense_type"
              value="2"
            />
            <span></span>
          </label>
          درآمد
        </div>
        <button
          type="button"
          className="btn btn-success"
          onClick={e => this.handelSave()}
        >
          ذخیره اطلاعات
        </button>
      </form>
    );
  }

}