import React from "react";
import AddUser from "./AddUser";
import Table from "./List/Table";

export default function ShowList(props) {

  const page = props.Page;
  const users = props.Users;
  const upDateUsers = props.upDateUsers;

  const Display = {
    list: "list",
    addUser: "addUser"
  };
  
  const displayPage = Display[page] ? (
    page === "list" ? (
      <Table Users={users} upDateUesrs={upDateUsers} />
    ) : (
      <AddUser Users={users} upDateUesrs={upDateUsers} />
    )
  ) : (
    <Table Users={[]} />
  );

  return <>{displayPage}</>;
}
