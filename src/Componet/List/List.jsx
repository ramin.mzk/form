import React, { Component } from "react";

export default class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      filter: "all"
    };
  }

  updateUesr = data => {
    const addUser = new Promise((resolve, reject) => {
      if (data) {
        this.setState(prevState => ({
          users: [...prevState.users, data]
        }));
        return resolve();
      }
      return reject();
    });

    addUser
      .then(() => {
        this.props.updatePage("list", this.state.users, 'all');
      })
      .catch(() => console.log("Error"));
  };

  filterItem = filter => {
    const Filter = new Promise((resolve, reject) => {
      if (filter) {
        this.setState({ filter: filter });
        return resolve();
      }
      return reject();
    });
    Filter.then(() =>
      this.props.updatePage("list", this.state.users, this.state.filter)
    ).catch(() => {
      console.log("Error");
    });
  };

  render() {
    const Display = this.props.Display;
    const updatePage = this.props.updatePage;

    return (
      <>
        <div className="card-body">
          <div className="actions">
            <button
              className="btn btn-outline-success btn-icon"
              type="button"
              onClick={e =>
                updatePage("addUser", this.state.users, this.updateUesr)
              }
            >
              <i className="material-icons"> note_add </i>
              <span>اضافه کردن</span>
            </button>
            <button
              className="btn btn-outline-primary  btn-icon"
              type="button"
              onClick={e =>
                updatePage("list", this.state.users, this.state.filter)
              }
            >
              <i className="material-icons">list</i>
              <span>لیست وظایف</span>
            </button>
            <div className="row" style={{ marginTop: "5px" }}>
              <div className="col">
                <div className="btn-group" role="group" aria-label="فیلتر کردن">
                  <button
                    type="button"
                    onClick={e => {
                      this.filterItem("all");
                    }}
                    className="btn btn-secondary"
                  >
                    همه
                  </button>
                  <button
                    type="button"
                    onClick={e => {
                      this.filterItem("outCome");
                    }}
                    className="btn btn-secondary"
                  >
                    هزینه
                  </button>
                  <button
                    type="button"
                    onClick={e => {     
                      this.filterItem("inCome");
                    }}
                    className="btn btn-secondary"
                  >
                    درآمد
                  </button>
                </div>
              </div>
            </div>
            <div className="card-body">{Display}</div>
          </div>
        </div>
      </>
    );
  }
}
