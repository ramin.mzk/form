import React, { Component } from "react";
import NOitem from "./NOitem";
import Items from "./Items";
import TheadAndTfoot from "./TheadAndTfoot";

export default class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    
    const Data = this.props.Users;
    const table =
      Data.length > 0 ? (
        <Items Users={Data} upDateUesrs={this.props.upDateUesrs} />
      ) : (
        <NOitem />
      );

    return (
      <table className="table table-bordered table-hover table-striped">
        <TheadAndTfoot />
        <tbody>{table}</tbody>
      </table>
    );
  }
}
