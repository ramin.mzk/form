import React, { Component } from "react";
import { getCategories } from "../../Utility/Category";
import Toman from "../../partials/Curency/Toman";

export default class Items extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  addUserLIst = (user, index) => {
    const category = getCategories();
    return (
      <tr key={index}>
        <td>{user.title}</td>
        <td>{category[user.category]}</td>
        <td>
          <Toman amount={user.amount} />
        </td>
        <td>
          <span
            className={
              user.type === "1" ? "badge badge-danger" : "badge badge-primary"
            }
          >
            {user.type === "1" ? "هزینه" : "درآمد"}
          </span>
        </td>
        <td>
          <i className="material-icons">edit</i>
          <i className="material-icons">delete</i>
        </td>
      </tr>
    );
  };

  render() {
    const users = this.props.Users;
    const filter = this.props.upDateUesrs;
    const usersFilter = require ("../../partials/users/filterUsers")(users, filter);
    
    return (
      <>
        {usersFilter.map((users, index) => this.addUserLIst(users, index))}
      </>
    );
  }
}
