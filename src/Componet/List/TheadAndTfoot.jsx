import React, { Component } from 'react'

export default class TheadAndTfoot extends Component {
         render() {
           return (
             <>
               <thead>
                 <tr>
                   <th>عنوان</th>
                   <th>دسته بندی</th>
                   <th>انجام دهنده</th>
                   <th>وضعیت</th>
                   <th>عملیات</th>
                 </tr>
               </thead>
               <tfoot>
                 <tr>
                   <th>عنوان</th>
                   <th>دسته بندی</th>
                   <th>انجام دهنده</th>
                   <th>وضعیت</th>
                   <th>عملیات</th>
                 </tr>
               </tfoot>
             </>
           );
         }
       }
