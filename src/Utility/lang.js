const numbers = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];

class lang {
  toPersion(input) {
    return String(input)
      .split('')
      .map(number => (numbers[number] ? numbers[number] : number))
      .join("");
  }
}

export default lang;
