import lang from "./lang";

class Currency {
  constructor() {
    this.lang = new lang()
  }

  formatAsToman(input) {
    return this.lang.toPersion(this.asCurrency(input)) + " تومان";
  }

  formatAsRial(input) {
    return this.lang.toPersion(this.asCurrency(input * 10)) + " ريال ";
  }

  formatAsHezarToman(input) {
    return this.lang.toPersion(this.asCurrency(input / 1000)) + " هزار تومان";
  }
  asCurrency(input) {
    return input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}

export default Currency;
