export const getCategories = () => {
  return {
    none: "انتحاب کنید",
    food: "غذا",
    salary: "حقوق",
    health: "سلامتی",
    home: "خانواده",
    bills: "قبوض"
  };
};
