const filterUsers = (users, filter) => {
  const usersFilter = item => {
    if (filter === "all") {
      return true;
    } else if (filter === "inCome" && item.type === "2") {
      return true;
    } else if (filter === "outCome" && item.type === "1") {
      return true;
    }
  };
  return users.filter(item => usersFilter(item));
};

module.exports= filterUsers
