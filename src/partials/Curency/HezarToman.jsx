import React from "react";
import Currency from "../../Utility/Currency";

export default function HezarToman(amount) {
  const hezarToman = new Currency();
  return (
    <div>
      <span>{hezarToman.formatAsHezarToman(amount)}</span>
    </div>
  );
}
