import React from "react";
import Currencuy from '../../Utility/Currency'

export default function Toman({ amount }) {
  const currency = new Currencuy();
  return (
    <div className="currency-Toman">
      <span>{currency.formatAsToman(amount)}</span>
    </div>
  );
}
