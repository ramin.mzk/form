import React from "react";
import Currency from "../../Utility/Currency";

export default function Rial({ amount }) {
  const Rial = new Currency();
  return (
    <div>
      <span>{Rial.formatAsRial(amount)}</span>
    </div>
  );
}
